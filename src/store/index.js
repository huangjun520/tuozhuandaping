import { createStore } from 'vuex'
import login from './modules/login'

export default createStore({
  strict:false,
  state: {
    uid:0,
    username:"未知",
    level:{},
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    login
  }
})
