//全局属性变量
const config={
    modelTimeOut:120,//数据模型默认请求时间 120s
    modelTimeOutMin:1,//数据模型默认最短请求时间 60s
    apiTimeOut:10,//api刷新时间，默认 10秒，最低1秒
    apiTimeOutMin:5,//api刷新时间，默认 10秒，最低1秒
}

//yemian 页面 
var yemianAttr={
    // areaWidth:window.innerWidth,//大屏屏幕宽度
    areaWidth:1000,//大屏屏幕宽度
    // areaHeight:window.innerHeight,//屏幕高度
    areaHeight:700,//屏幕高度
    areaBg:'#0e2a43',//画布背景颜色 
    yemianSize:1,//显示类型：默认固定尺寸
    guideX:"",//X轴辅助线
    guideY:"",//Y轴辅助线
};

//group 图层组 
var groupAttr={
    id:1,
    pid:0,
    dataVisualizationId:0,
    label:"图层组",
    name:"group",
    active:false,
    draggable:true,
    resizable:false,
    width:200,
    height:60,
    x:100,
    y:100,
    px:0,
    py:0,
    zindex:1,
    showZIndex:1,
    display:"block",
    style:{
        open:false,//是否展开组内的图层
    },
    data:[{content:"图层组",url:''}],
    child:[],
};

//id 1 文本组件
var textAttr={
    id:1,
    pid:0,
    dataVisualizationId:0,
    label:"文本组件",
    name:"text",
    active:false,
    draggable:true,
    resizable:true,
    width:400,
    height:100,
    x:100,
    y:100,
    px:0,
    py:0,
    zindex:1,
    showZIndex:1,
    display:"block",
    style:{
        layoutType:1,//类型：1 PC端 2 移动端 3 PC端响应式
        //基本
        rotate:0,//旋转角度
        opacity:100,//透明度

        //文本
        fontFamily:"Microsoft YaHei",
        fontWeight:400,
        fontSize:16,
        lineHeight:40,
        textAlign:'left',
        color:"#fff",
        padding:0,

        //外观
        borderWidth:1,//
        borderStyle:"none",
        borderRadius:0,
        borderColor:"#fff",
        backBgON:false,
        backBg:"#333",
        backOpacity:100,//背景透明度
        backRGBA:"rgba(51, 51, 51,1)",//背景RGBA形式的值

        //动画
        isAnimate:false,//是否开启文字滚动动画
        behavior:"scroll",//滚动方式：alternate 来回滚动、scroll 循环滚动、slide只滚动一次
        direction:"left",//滚动的方向:down、left、right、up
        scrollamount:6,//运动速度

        //数据
        dataType:"static",//数据类型：静态数据【默认】、api接口、数据模型
        dataYField:"txt",//Y轴 数据字段
        dataFields:'txt',//字段列表
    },
    data:'[{ "txt": "https://www.51qingtian.com/ 情天可视化平台是基于SpringBoot+Vue3+mysql构建的数据可视化开发平台"}]',
};

var lists=[
    {
        group:"图表组件",
        num:14,
        icon:"iconzhuxingtu",
        show:true,
        list:[
            {
                group:"全部",
                num:18,
                icon:"iconquanbu_huaban",
                show:true,
                list:[{
                    id:1,
                    name:"text",
                    version:"v1.0.9",
                    attr:textAttr,
                    label:"文本组件",
                    icon:"text/text.png"
                },{
                    id:1,
                    name:"text",
                    version:"v1.0.9",
                    attr:textAttr,
                    label:"文本组件",
                    icon:"text/text.png"
                },]
            },
        ]
    },{
        group:"文字组件",
        num:4,
        icon:"iconwenzi",
        show:false,
        list:[
            {
                group:"全部",
                num:8,
                icon:"iconquanbu_huaban",
                show:true,
                list:[{
                    id:1,
                    name:"text",
                    version:"v1.0.9",
                    attr:textAttr,
                    label:"文本组件",
                    icon:"text/text.png"
                },]  
            }
        ]
    },
]

var getWidgetById=function(id){
    var rs=null;
    //根list
    for (let i = 0; i < lists.length; i++) {
        if(rs!=null){
            break;
        }
        var list=lists[i].list;
        //组件大类
        for (let n = 0; n < list.length; n++) {
            if(rs!=null){
                break;
            }
            var group = list[n].list;
            //组件小类
            for (let m = 0; m < group.length; m++) {
                var widget = group[m];
                //组件
                if(widget.id==id){
                    rs=JSON.parse(JSON.stringify(widget));
                    break;
                }
            }
        }
    }
    return rs;
}
var getWidgetByName=function(name){
    var rs=null;
    //根list
    for (let i = 0; i < lists.length; i++) {
        if(rs!=null){
            break;
        }
        var list=lists[i].list;
        //组件大类
        for (let n = 0; n < list.length; n++) {
            if(rs!=null){
                break;
            }
            var group = list[n].list;
            //组件小类
            for (let m = 0; m < group.length; m++) {
                var widget = group[m];
                //组件
                if(widget.name==name){
                    rs=JSON.parse(JSON.stringify(widget));
                    break;
                }
            }
        }
    }
    return rs;
}
//获得交互组件
var getTnteractiveList=function(){
    var rs=null;
    for (let i = 0; i < lists.length; i++) {
        if(lists[i].group=="交互控制组件"){
            rs=lists[i].list[0].list;
            break;
        }
    }
    return rs;
}

export default {
    config:config,//
    //所有的组件列表
    lists:lists,
    getWidgetById:getWidgetById,
    getWidgetByName:getWidgetByName,
    getTnteractiveList:getTnteractiveList,
    groupAttr:groupAttr,
    yemianAttr:yemianAttr,
}

