//字体样式 -- 下划线
var textDecoration=[
  {
    label: '默认(无)',
    value: 'none'
  },
  {
    label: '上划线',
    value: 'overline'
  },
  {
    label: '中划线',
    value: 'line-through'
  },
  {
    label: '下划线',
    value: 'underline'
  }
]


//flex布局 -- 方向
var flexDirection=[
  {
    label: '水平方向',
    value: 'row'
  },
  {
    label: '垂直方向',
    value: 'column'
  }
]

//交互组件 -- 参数条件
var interactiveWhere=[
  {
    label: '等于',
    value: '=='
  },
  {
    label: '大于',
    value: '>'
  },
  {
    label: '小于',
    value: '<'
  },
  {
    label: '大于等于',
    value: '>='
  },
  {
    label: '小于等于',
    value: '<='
  }
]

//qSelect -- 下拉框弹出方向
var qSelectDirection=[
  {
    label: '自动',
    value: 'auto'
  },
  {
    label: '向下',
    value: 'down'
  },
  {
    label: '向上',
    value: 'up'
  }
]


//地图---地图样式
var mapStyle=[
  {
    label: '标准',
    value: 'normal'
  },
  {
    label: '幻影黑',
    value: 'dark'
  },
  {
    label: '月光银',
    value: 'light'
  },
  {
    label: '远山黛',
    value: 'whitesmoke'
  },
  {
    label: '草色青',
    value: 'fresh'
  },
  {
    label: '雅士灰',
    value: 'grey'
  },
  {
    label: '涂鸦',
    value: 'graffiti'
  },
  {
    label: '马卡龙',
    value: 'macaron'
  },
  {
    label: '靛青蓝',
    value: 'blue'
  },
  {
    label: '极夜蓝',
    value: 'darkblue'
  },
  {
    label: '酱籽',
    value: 'wine'
  }
]

//地图---地理范围
var areaLevel=[
  {
    label: '全球',
    value: 1
  },
  {
    label: '中国',
    value: 2
  },
  {
    label: '省级',
    value: 3
  },
  {
    label: '市级',
    value: 4
  },
]


//数据模型--定时抽取--时间间隔
var extractTimeOut=[
  {
    label: '1分钟',
    value: 60
  },
  {
    label: '5分钟',
    value: 300
  },
  {
    label: '10分钟',
    value: 600
  },
  {
    label: '30分钟',
    value: 1800
  },
  {
    label: '1小时',
    value: 3600
  },
  {
    label: '3小时',
    value: 10800
  },
  {
    label: '6小时',
    value: 21600
  },
  {
    label: '12小时',
    value: 43200
  },
  {
    label: '1天',
    value: 86400
  },

]

//页面寸尺
var yemianSize=[
  {
    label: '固定尺寸',
    value: 1
  },
  {
    label: '宽高-自适应',
    value: 2
  },
  {
    label: '宽-自适应',
    value: 3
  },
  {
    label: '高-自适应',
    value: 4
  },
]

//DataV--表格--轮播方式
var tableCarousel= [
  {
    label: '单行滚动',
    value: 'single'
  },
  {
    label: '整页滚动',
    value: 'page'
  },
];

//媒体组件--图片--填充方式
var imgFit= [
  {
    label: '自适应内容',
    value: 'contain'
  },
  {
    label: '拉伸铺满',
    value: 'fill'
  },
];

//DataV--动画曲线
var animationCurve= [
  {
    label: 'linear',
    value: 'linear'
  },
  {
    label: 'easeInSine',
    value: 'easeInSine'
  },
  {
    label: 'easeOutSine',
    value: 'easeOutSine'
  },
  {
    label: 'easeInOutSine',
    value: 'easeInOutSine'
  },
  {
    label: 'easeInQuad',
    value: 'easeInQuad'
  },
  {
    label: 'easeOutQuad',
    value: 'easeOutQuad'
  },
  {
    label: 'easeInOutQuad',
    value: 'easeInOutQuad'
  },
  {
    label: 'easeInCubic',
    value: 'easeInCubic'
  },
  {
    label: 'easeOutCubic',
    value: 'easeOutCubic'
  },
  {
    label: 'easeInOutCubic',
    value: 'easeInOutCubic'
  },
  {
    label: 'easeInQuart',
    value: 'easeInQuart'
  },
  {
    label: 'easeOutQuart',
    value: 'easeOutQuart'
  },
  {
    label: 'easeInOutQuart',
    value: 'easeInOutQuart'
  },
  {
    label: 'easeInQuint',
    value: 'easeInQuint'
  },
  {
    label: 'easeOutQuint',
    value: 'easeOutQuint'
  },
  {
    label: 'easeInOutQuint',
    value: 'easeInOutQuint'
  },
  {
    label: 'easeInBack',
    value: 'easeInBack'
  },
  {
    label: 'easeOutBack',
    value: 'easeOutBack'
  },
  {
    label: 'easeInOutBack',
    value: 'easeInOutBack'
  },
  {
    label: 'easeInElastic',
    value: 'easeInElastic'
  },
  {
    label: 'easeOutElastic',
    value: 'easeOutElastic'
  },
  {
    label: 'easeInOutElastic',
    value: 'easeInOutElastic'
  },
  {
    label: 'easeInBounce',
    value: 'easeInBounce'
  },
  {
    label: 'easeOutBounce',
    value: 'easeOutBounce'
  },
  {
    label: 'easeInOutBounce',
    value: 'easeInOutBounce'
  },
];

//大屏--数据模型--聚合函数
var modelFunc= [
  {
    label: '无',
    value: 'none'
  },
  {
    label: '平均值',
    value: 'avg'
  },
  {
    label: '最大值',
    value: 'max'
  },
  {
    label: '最小值',
    value: 'min'
  },
  {
    label: '求和',
    value: 'sum'
  },
  {
    label: '求行数',
    value: 'count'
  },
];

//大屏--数据类型
var dataType= [
  {
    label: '静态数据',
    value: 'static'
  },
  {
    label: 'api接口',
    value: 'api'
  },
  {
    label: '数据模型',
    value: 'model'
  },
];


//仪表盘  详情位置
var detailsPosition=[
  {
    label: '开始位置',
    value: 'start'
  },
  {
    label: '中间位置',
    value: 'center'
  },
  {
    label: '结束位置',
    value: 'end'
  },
]

//水位图 形状
var waterShape=[
  {
    label: '矩形',
    value: 'rect'
  },
  {
    label: '圆角矩形',
    value: 'roundRect'
  },
  {
    label: '圆形',
    value: 'round'
  },
]

//图例 位置
var legendPosition= [
  {
    label: '顶部',
    value: 'top'
  },
  {
    label: '顶左部',
    value: 'top-left'
  },
  {
    label: '顶右部',
    value: 'top-right'
  },
  {
    label: '右部',
    value: 'right'
  },
  {
    label: '右顶部',
    value: 'right-top'
  },
  {
    label: '右底部',
    value: 'right-bottom'
  },
  {
    label: '左部',
    value: 'left'
  },
  {
    label: '左顶部',
    value: 'left-top'
  },
  {
    label: '左底部',
    value: 'left-bottom'
  },
  {
    label: '底部',
    value: 'bottom'
  },
  {
    label: '底左部',
    value: 'bottom-left'
  },
  {
    label: '底右部',
    value: 'bottom-right'
  },
];


//值标签 位置
var labelPosition= [
  {
    label: '顶部',
    value: 'top'
  },
  {
    label: '中间',
    value: 'middle'
  },
  {
    label: '底部',
    value: 'bottom'
  },
];

//折线曲率
var lineCurvature= [
  {
    label: '折线',
    value: 'circle'
  },
  {
    label: '平滑曲线',
    value: 'smooth'
  }
];

//字体
var fontFamily=[
  {
    label: '宋体',
    value: 'SimSun'
  },
  {
    label: '黑体',
    value: 'SimHei'
  },
  {
    label: '微软雅黑',
    value: 'Microsoft YaHei'
  },
  {
    label: '仿宋',
    value: 'FangSong'
  },
  {
    label: '新宋体',
    value: 'NSimSun'
  },
  {
    label: '楷体',
    value: 'KaiTi'
  },
  {
    label: '幼圆',
    value: 'YouYuan'
  },
  {
    label: '隶书',
    value: 'LiSu'
  },
  {
    label: '细明体',
    value: 'MingLiU'
  },
  {
    label: '新细明体',
    value: 'PMingLiU'
  },
  {
    label: '华文彩云',
    value: 'STCaiyun'
  },
  {
    label: '华文新魏',
    value: 'STXinwei'
  },
  {
    label: '华文行楷',
    value: 'STXingkai'
  },
  {
    label: '华文隶书',
    value: 'STLiti'
  },
  {
    label: '华文琥珀',
    value: 'STHupo'
  },
  {
    label: '华文宋体',
    value: 'STSong'
  },
  {
    label: '华文楷体',
    value: 'STKaiti'
  },
  {
    label: '华文黑体',
    value: 'STHeiti'
  },
  {
    label: '华文细黑',
    value: 'STHeiti Light [STXihei]'
  },
  {
    label: '方正姚体',
    value: 'FZYaoti'
  },
  {
    label: '方正舒体',
    value: 'FZShuTi'
  },
  {
    label: 'Arial',
    value: 'Arial'
  },
  {
    label: 'Helvetica',
    value: 'Helvetica'
  },
  {
    label: 'Tahoma',
    value: 'Tahoma'
  },
  {
    label: 'Verdana',
    value: 'Verdana'
  },
  {
    label: 'Lucida Grande',
    value: 'Lucida Grande'
  },
  {
    label: 'Times New Roman',
    value: 'Times New Roman'
  },
  {
    label: 'Georgia',
    value: 'Georgia'
  },
]
var fontWeights = [
  {
    label: 'Normal',
    value: 'normal'
  },
  {
    label: 'Bold',
    value: 'bold'
  },
  {
    label: 'Bolder',
    value: 'bolder'
  },
  {
    label: 'Lighter',
    value: 'lighter'
  },
  {
    label: '100',
    value: '100'
  },
  {
    label: '200',
    value: '200'
  },
  {
    label: '300',
    value: '300'
  },
  {
    label: '400',
    value: '400'
  },
  {
    label: '500',
    value: '500'
  },
  {
    label: '600',
    value: '600'
  },
  {
    label: '700',
    value: '700'
  },
  {
    label: '800',
    value: '800'
  }
];

var textAligns = [
  {
    label: '左对齐',
    value: 'left'
  },
  {
    label: '居中对齐',
    value: 'center'
  },
  {
    label: '右对齐',
    value: 'right'
  },
];

//边框
var borderStyles = [
  {
    label: '实线',
    value: 'solid'
  },
  {
    label: '虚线',
    value: 'dashed'
  },
  {
    label: '点线',
    value: 'dotted'
  },
  {
    label: '双线',
    value: 'double'
  },
  {
    label: '凹槽边框',
    value: 'groove'
  },
  {
    label: '垄状边框',
    value: 'ridge'
  },
  {
    label: 'inset 边框',
    value: 'inset'
  },
  {
    label: 'outset 边框',
    value: 'outset'
  },
  {
    label: '空',
    value: 'none'
  },
];

var display = [
  {
    label: '显示',
    value: 'block'
  },
  {
    label: '隐藏',
    value: 'none'
  }
];

var layoutSchemes = [
  {
    label: '大屏专用布局',
    value: 'AbsoluteLayoutCanvas'
  }
];

var cursor = [
  {
    label: '默认',
    value: 'default'
  },
  {
    label: '指针',
    value: 'pointer'
  }
];

var canvasGridClass = [
  {
    label: '5px * 5px 白色',
    value: 'canvas-grid-white-5px5px'
  },
  {
    label: '5px * 5px 黑色',
    value: 'canvas-grid-black-5px5px'
  },
  {
    label: '10px * 10px 白色',
    value: 'canvas-grid-white-10px10px'
  },
  {
    label: '10px * 10px 黑色',
    value: 'canvas-grid-black-10px10px'
  },
  {
    label: '20px * 20px 白色',
    value: 'canvas-grid-white-20px20px'
  },
  {
    label: '20px * 20px 黑色',
    value: 'canvas-grid-black-20px20px'
  }
];

var pixelUnits = [
  {
    label: 'px',
    value: 'px'
  },
  {
    label: '%',
    value: '%'
  },
  /*{
    label: 'em',
    value: 'em'
  },
  {
    label: 'rem',
    value: 'rem'
  },*/
];

var backgroundRepeats = [
  {
    label: '不重复',
    value: 'no-repeat'
  },
  {
    label: '重复',
    value: 'repeat'
  },
  {
    label: '横轴重复',
    value: 'repeat-x'
  },
  {
    label: '纵轴重复',
    value: 'repeat-y'
  },
];

var canvasSizes = [
  {
    label: 'IPhone 6/7/8',
    value: '375*667*px*px'
  },
  {
    label: 'IPhone 6/7/8 plus',
    value: '414*736*px*px'
  },
  {
    label: 'IPhone X',
    value: '375*812*px*px'
  },
  {
    label: 'IPad',
    value: '768*1024*px*px'
  },
  {
    label: 'IPad Pro',
    value: '1024*1366*px*px'
  },
];

var canvasSizes2 = [
  {
    label: '全屏',
    value: '100*100*%*%'
  },
  {
    label: '1024px * 768px',
    value: '1024*768*px*px'
  },
  {
    label: '1440px * 900px',
    value: '1440*900*px*px'
  },
  {
    label: '1680px * 1050px',
    value: '1680*1050*px*px'
  },
  {
    label: '1920px * 1080px',
    value: '1920*1080*px*px'
  }
];

var iframeScrollings = [
  {
    label: '显示',
    value: 'yes'
  },
  {
    label: '不显示',
    value: 'no'
  },
  {
    label: '自动',
    value: 'auto'
  },
];



var writingModes = [
  {
    label: '水平',
    value: 'horizontal-tb'
  },
  {
    label: '垂直',
    value: 'vertical-rl'
  },
];

var overflows = [
  {
    label: '自动',
    value: 'auto'
  },
  {
    label: '隐藏',
    value: 'hidden'
  }
];

/**
 *
 * @type {*[]}
 */
 var targets = [
  {
    label: '当前页面跳转',
    value: '_self'
  },
  {
    label: '新窗口',
    value: '_blank'
  }
];

export default {
  borderStyles:borderStyles,
  display:display,
  layoutSchemes:layoutSchemes,
  cursor:cursor,
  canvasGridClass:canvasGridClass,
  pixelUnits:pixelUnits,
  backgroundRepeats:backgroundRepeats,
  canvasSizes:canvasSizes,
  canvasSizes2:canvasSizes2,
  iframeScrollings:iframeScrollings,
  fontWeights:fontWeights,
  textAligns:textAligns,
  writingModes:writingModes,
  overflows:overflows,
  targets:targets,
  fontFamily:fontFamily,
  labelPosition:labelPosition,
  dataType:dataType,
  modelFunc:modelFunc,
  lineCurvature:lineCurvature,
  legendPosition:legendPosition,
  animationCurve:animationCurve,
  tableCarousel:tableCarousel,
  yemianSize:yemianSize,
  extractTimeOut:extractTimeOut,
  imgFit:imgFit,
  areaLevel:areaLevel,
  mapStyle:mapStyle,
  detailsPosition:detailsPosition,
  waterShape:waterShape,
  qSelectDirection:qSelectDirection,
  interactiveWhere:interactiveWhere,
  flexDirection:flexDirection,
  textDecoration:textDecoration
}
