import { createApp } from 'vue'
// import { createApp } from 'vue/dist/vue.esm-bundler.js'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus';
import 'dayjs/locale/zh-cn'
import locale from 'element-plus/lib/locale/lang/zh-cn'

import Vue3DraggableResizable from 'vue3-draggable-resizable'
//需引入默认样式
import 'vue3-draggable-resizable/dist/Vue3DraggableResizable.css'


import 'element-plus/lib/theme-chalk/index.css';
// 引入阿里云图标
import './assets/iconfont/iconfont.css';
// 引入阿里云图标
import './assets/js/jquery-1.7.1.min.js';



//兼容IE 11
import 'babel-polyfill'
import Es6Promise from 'es6-promise'
require('es6-promise').polyfill()
Es6Promise.polyfill()

//创建vue
const vue=createApp(App);

// 自动扫描组件
import loadWidget from './widgets/index.js'
loadWidget(vue);


vue.use(store)
.use(ElementPlus, { locale })
.use(router)
.use(Vue3DraggableResizable)
.mount('#app')
