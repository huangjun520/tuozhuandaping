export default {
    methods:{
        //点击图层：改变图层的活动状态
        tucengClick(e,widget){
            // return false;
            if (e.button != 0) {
                console.log("不是鼠标左键!")
                return false;
            }
            //按住shift键，可以快速多选
            if (e.shiftKey==1){
                // alert("shift被按下了")
                return false;
            }
            // 按住 Ctrl 时，可以多选  
            if (e.ctrlKey) {
                widget.active=!widget.active;
                return false;
            }
            
            for(var i=0; i<this.widgetList.length;i++){
                //其他图层都变为不活动状态
                if(this.widgetList[i].active==true){
                    this.widgetList[i].active=false;
                    // break;
                }
                //分组中的其他图层也变成不活动的状态
                if(this.widgetList[i].child && this.widgetList[i].child.length>0){
                    var child=this.widgetList[i].child;
                    for(var n=0; n<child.length;n++){
                        child[n].active=false;
                    }
                }
                //该图层的父级【分组】，变为不活动状态
                if(this.widgetList[i].id==widget.pid){
                    this.widgetList[i].showZIndex=0;
                }
            }
            widget.active=true;
        },
    }
}