export default {
    watch:{
        'attrs.style.legendColorList':{
            handler(newVal, oldVal){
                console.log(newVal)
                this.setDefaultStyle();
            },
        },
        'attrs.width':{
            handler(newVal, oldVal){
                // console.log(newVal)
                // this.setDefaultStyle();
                if(this.$refs['border'+this.attrs.id]) this.$refs['border'+this.attrs.id].initWH();
            },
        },
        'attrs.height':{
            handler(newVal, oldVal){
                // console.log(newVal)
                // this.setDefaultStyle();
                if(this.$refs['border'+this.attrs.id]) this.$refs['border'+this.attrs.id].initWH();
            },
        },
      }
}