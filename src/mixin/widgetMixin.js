export default {
    methods:{
        //修改图层基本属性，不包括style和data
        editWidget(){
            var t;
            // console.log("editWidget blur")
            if(t)clearTimeout(t);
            t=setTimeout(() => {
                // console.log("editWidget:"+this.attrs.id)
                this.$emit('editWidget',this.attrs);//保存图层属性数据至数据库
            }, 2000);
            
        },
        //修改图层style属性
        editWidgetStyle(){
            // console.log("editWidget blur")
            this.$emit('editWidgetStyle',this.attrs);//保存图层style属性数据至数据库
        },
        //修改图层data属性
        editWidgetData(){
            // console.log("editWidget blur")
            this.$emit('editWidgetData',this.attrs);//保存图层属性数据至数据库
        },
        //加载数据模型列表
        getModelList(){
            this.$emit('getModelList');//保存图层属性数据至数据库
        },
        //编辑数据
        editWidgetDataBefore(dataFieldList){
            //coding.....
            console.log("dataFieldList:"+dataFieldList);
            if(dataFieldList) this.attrs.style.dataFields=dataFieldList;
            this.editWidgetData();
        },
        //解析dataFields为对象数组
        parseDataFields(){
            var arr=this.attrs.style.dataFields.split(',');
            if(arr.length>0){
                this.dataFieldList=[];
                for(var i=0;i<arr.length;i++){
                    var obj={
                        value:arr[i],
                        label:arr[i]
                    }
                    this.dataFieldList[i]=obj;
                }
            }
        },
        //加载字段列表
        loadFields(){
            if(this.loadFieldsTime) clearTimeout(this.loadFieldsTime);
            this.loadFieldsTime=setTimeout(() => {
                if(this.attrs.style.dataType=="static") this.parseDataFields();
                if(this.attrs.style.dataType=="model") this.loadModelFields();
                if(this.attrs.style.dataType=="api") this.loadApiFields();
                if(this.attrs.style.dataType=="sql") this.loadSqlFields();
            }, 500);
        },
    }
}