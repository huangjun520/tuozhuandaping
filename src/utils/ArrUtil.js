
/**
 * 数组工具类
 */

//两个元素换位子
var swapArr=function(arr, index1, index2){
    arr[index1] = arr.splice(index2, 1, arr[index1])[0];
    return arr;
}

//置顶移动
var toFirst=function(fieldData,index){
    // if(index!=0){

    //     // fieldData[index] = fieldData.splice(0, 1, fieldData[index])[0]; 这种方法是与另一个元素交换了位子，

    //     fieldData.unshift(fieldData.splice(index , 1)[0]);

    // }
    fieldData.unshift(fieldData.splice(index,1)[0]);
}

//置底移动
var toLast=function(fieldData,index){
    fieldData.push(fieldData.splice(index,1)[0]);
}

//up 上移动一格
var upGo=function(fieldData,index){
    if(index!=0){

        fieldData[index] = fieldData.splice(index-1, 1, fieldData[index])[0];

    }
    // else{
    //     //已经在最顶上时，移动至末尾
    //     fieldData.push(fieldData.shift());

    // }
}

//down 下移动一格
var downGo=function(fieldData,index){
    if(index!=fieldData.length-1){

        fieldData[index] = fieldData.splice(index+1, 1, fieldData[index])[0];

    }
    // else{
    //     //已经在最底下时，移动至顶部
    //     fieldData.unshift( fieldData.splice(index,1)[0]);
    // }
}

//判断某个元素是否在数组中，存在返回true
var inArr=function(arr,obj){
    for (let i = 0; i < arr.length; i++) {
        if(arr[i]==obj){
            return true;
        }
    }

    return false;
}
//判断某个元素是否在数组中，存在返回index,不存在返回的index=-1
var inArr2=function(arr,obj){
    var index=-1;
    for (let i = 0; i < arr.length; i++) {
        if(arr[i]==obj){
            index=i;
            break;
        }
    }
    return index;
}

//判断某个对象属性是否在对象数组中，存在返回true
var inObjArr=function(arr,key,value){
    for (let i = 0; i < arr.length; i++) {
        if(arr[i][key]==value){
            return true;
        }
    }

    return false;
}

//根据id，获得widget在数组中的索引
var getIndexBYId=function(arr,id){
    for (let i = 0; i < arr.length; i++) {
        if(arr[i].id==id){
            return i;
        }
    }

    return -1;
}

//根据id，获得数组中的widget
var getWidgetBYId=function(arr,id){
    for (let i = 0; i < arr.length; i++) {
        if(arr[i].id==id){
            return arr[i];
        }
    }

    return null;
}

//json对象数组按对象属性排序 
var sortJson=function(arr,field,type="ASC"){
    arr.sort(function(a,b){
        if(type=="ASC"){
            return a[field]-b[field];
        }else{
            return b[field]-a[field];
        }
    });
}

//获得某个字段的最大值
var getMaxOfKey=function(arr,field){
    var max=null;
    for (let i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if(max==null){
            max=obj[field];
        }else{
            max=max>=obj[field]?max:obj[field];
        }
    }
    max=max==null?1:max;
    return max;
}

//获得某个字段的最小值
var getMinOfKey=function(arr,field){
    var min=null;
    for (let i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if(min==null){
            min=obj[field];
        }else{
            min=min<=obj[field]?min:obj[field];
        }
    }
    min=min==null?1:min;
    return min;
}
//获得某个字段对应的另一个字段的值
var getValueOfKey=function(arr,field,fieldValue,value){
    var rs=null;
    for (let i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if(obj[field]==fieldValue){
            rs=obj[value];
            break;
        }
    }
    return rs;
}

//获得某个字段对应的另一个字段的值
var getValueOfKey2=function(arr,field,fieldValue,value){
    var rs=null;
    for (let i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if(obj[field]==fieldValue || fieldValue.indexOf(obj[field])>=0){
            rs=obj[value];
            break;
        }
    }
    return rs;
}

//1.功能：将浮点数四舍五入，取小数点后N位   
function toDecimal(x,n) {   
    var f = parseFloat(x);   
    if (isNaN(f)) {   
        return;   
    }   
    var nn=10;
    for (let i = 1; i <= n; i++) {
        nn=nn*10;
    }
    f = Math.round(x*nn)/nn;   
    return f;   
} 

//功能：将数组中的每个元素，强制转换为数字 
function toNumber(arr) {   
    for (let i = 0; i < arr.length; i++) {
        arr[i]= parseFloat(arr[i]);
    }
    return arr;   
} 

export default {
    swapArr:swapArr,
    toFirst:toFirst,
    upGo:upGo,
    downGo:downGo,
    inArr:inArr,
    inArr2:inArr2,
    inObjArr:inObjArr,
    getIndexBYId:getIndexBYId,
    getWidgetBYId:getWidgetBYId,
    toLast:toLast,
    sortJson:sortJson,
    getMaxOfKey:getMaxOfKey,
    getMinOfKey:getMinOfKey,
    getValueOfKey:getValueOfKey,
    getValueOfKey2:getValueOfKey2,
    toDecimal:toDecimal,
    toNumber:toNumber,
}