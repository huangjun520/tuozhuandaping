
/**
 * 颜色工具类
 */

//16进制转换为RGB【返回字符串】
var toRGB=function (str) {
    // 16进制颜色值的正则
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    // 把颜色值变成小写
    var color = str.toLowerCase();
    if (reg.test(color)) {
      // 如果只有三位的值，需变成六位，如：#fff => #ffffff
      if (color.length === 4) {
        var colorNew = "#";
        for (var i = 1; i < 4; i += 1) {
          colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1));
        }
        color = colorNew;
      }
      // 处理六位的颜色值，转为RGB
      var colorChange = [];
      for (var i = 1; i < 7; i += 2) {
        colorChange.push(parseInt("0x" + color.slice(i, i + 2)));
      }
      return "rgb(" + colorChange.join(",") + ")";
    } else {
      return color;
    }
  }

//16进制转换为RGB【返回数组】
var toRGB2=function (str) {
    // 16进制颜色值的正则
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    // 把颜色值变成小写
    var color = str.toLowerCase();
    if (reg.test(color)) {
      // 如果只有三位的值，需变成六位，如：#fff => #ffffff
      if (color.length === 4) {
        var colorNew = "#";
        for (var i = 1; i < 4; i += 1) {
          colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1));
        }
        color = colorNew;
      }
      // 处理六位的颜色值，转为RGB
      var colorChange = [];
      for (var i = 1; i < 7; i += 2) {
        colorChange.push(parseInt("0x" + color.slice(i, i + 2)));
      }
      return colorChange;
    } else {
      return color;
    }
  }

//RGB转换为16进制【返回字符串】
  var toHex=function (rgb) {
    var _this = rgb;
       var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
       if(/^(rgb|RGB)/.test(_this)){
           var aColor = _this.replace(/(?:(|)|rgb|RGB)*/g,"").split(",");
           var strHex = "#";
           for(var i=0; i<aColor.length; i++){
               var hex = Number(aColor[i]).toString(16);
               hex = hex<10 ? 0+''+hex :hex;// 保证每个rgb的值为2位
               if(hex === "0"){
                   hex += hex;
               }
               strHex += hex;
           }
           if(strHex.length !== 7){
               strHex = _this;
           }
           return strHex;
       }else if(reg.test(_this)){
           var aNum = _this.replace(/#/,"").split("");
           if(aNum.length === 6){
               return _this;
           }else if(aNum.length === 3){
               var numHex = "#";
               for(var i=0; i<aNum.length; i+=1){
                   numHex += (aNum[i]+aNum[i]);
               }
               return numHex;
           }
       }else{
           return _this;
       }
  }

//16进制转换为RGBA【返回字符串】
var toRGBA=function (str,opacity) {
    // 16进制颜色值的正则
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    // 把颜色值变成小写
    var color = str.toLowerCase();
    if (reg.test(color)) {
      // 如果只有三位的值，需变成六位，如：#fff => #ffffff
      if (color.length === 4) {
        var colorNew = "#";
        for (var i = 1; i < 4; i += 1) {
          colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1));
        }
        color = colorNew;
      }
      // 处理六位的颜色值，转为RGB
      var colorChange = [];
      for (var i = 1; i < 7; i += 2) {
        colorChange.push(parseInt("0x" + color.slice(i, i + 2)));
      }
      return "rgba(" + colorChange.join(",") + ","+(opacity/100)+")";
    } else {
      return color;
    }
  }

export default {
    toRGB:toRGB,
    toRGB2:toRGB2,
    toHex:toHex,
    toRGBA:toRGBA,
}

