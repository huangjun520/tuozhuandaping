/**
 * 深拷贝对象
 * @param obj
 * @returns {any}
 */
 const cloneDeep = function (obj) {
    // let objStr = JSON.stringify(obj);
    // let cloneObj = JSON.parse(objStr);
    // return cloneObj
    return _.cloneDeep(obj)
  };
  
  /**
   * 深拷贝数组
   * @param arr
   * @returns {*}
   */
  const recursiveClone = function (arr) {
    return Array.isArray(arr) ? Array.form(arr, recursiveClone) : arr;
  };

  //获得json数组的 字段列表
  const getFieldList=function(jsonArr){
    //解析字段
    var dataFieldList=[];
    try{
        //解析第一行的字段
        var i=0;
        for(var key in jsonArr[0]){
            dataFieldList[i]=key;
            i++;
        }
    }catch(e){
        //解析失败，什么都不做
        dataFieldList=[];
    }
    return dataFieldList;
  }

  //获得json对象的第一个key
  const getFirstKey=function(obj){
    for (var key in obj) {
        return key;
    }
  }

  //校验 json数组格式：算法：前10行的第一个key值相同
  const checkJsonArr=function(jsonArr){
    var yes=true;
    try {
      var num=jsonArr.length>=10?10:jsonArr.length;//最多前10行
      var key=getFirstKey(jsonArr[0]);
      for (let i = 1; i < num; i++) {
        var k=getFirstKey(jsonArr[i]);
        if(key != k){
          yes=false;
          break;
        }
      }
    } catch (error) {
      yes=false;
    }
    
    return yes;
  }

  const checkURL=function(URL){
    var str=URL;
    //判断URL地址的正则表达式为:http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?
    //下面的代码中应用了转义字符"\"输出一个字符"/"
    var Expression=/http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
    var objExp=new RegExp(Expression);
    if(objExp.test(str)==true){
      return true;
    }else{
      return false;
    }
  }

  export default {
    cloneDeep,
    recursiveClone,
    getFieldList,
    getFirstKey,
    checkJsonArr,
    checkURL,
  }
  