    import network from 'axios'
    import router from '@/router'
    import URL from '@/api/apiUrl'
    const Qs = require('qs');

      // 1.创建axios的实例
      const instance = network.create({
        baseURL: URL.root,//本地开发
        timeout: 50000,
      });

    //   instance.defaults.withCredentials = true
  
      // 配置请求和响应拦截
      instance.interceptors.request.use(config => {
        // console.log('来到了request拦截success中');
        // 1.当发送网络请求时, 在页面中添加一个loading组件, 作为动画
  
        // 2.某些请求要求用户必须登录, 判断用户是否有token, 如果没有token跳转到login页面
        const token = window.localStorage.getItem('token')||''
        if(token) {
            config.headers['token'] = token
            
        }
        
        //支持post模拟form提交请求 multipart/form-data
        // config.headers['Content-type'] = 'application/x-www-form-urlencoded';
        // config.headers['Content-type'] = 'multipart/form-data';
        // config.transformRequest = [function (data) {
        //     let ret = ''
        //     for (let it in data) {
        //     ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
        //     }
        //     return ret
        // }];
        // 3.对请求的参数进行序列化(看服务器是否需要序列化)
        //如果不是文件上传，就走序列化
        // console.log("config.data",config.data);
        if(config.data instanceof FormData){
          console.log("文件上传")
        }else{
          config.data = Qs.stringify(config.data) //支持post模拟form提交请求
        }
        
  
        // 4.等等
        return config
      }, err => {
        // console.log('来到了request拦截failure中');
        return Promise.reject(err)
      })
  
      instance.interceptors.response.use(response => {
        //   console.log(response)
        // console.log('来到了response拦截success中');
        if(response.data.code==2001) {
            return router.push('/login');
          }
        return response.data
      }, err => {
        console.log('来到了response拦截failure中');
        console.log(err);
        if (err && err.response) {
          switch (err.response.status) {
            case 400:
              err.message = '请求错误'
              break
            case 401:
              err.message = '未授权的访问'
              break
          }
        }
        return err
      })

    export default instance
  