import { createRouter, createWebHashHistory } from 'vue-router'


const routes = [
  {
    path: '/',
    redirect:"/visualization",
    meta: {
        title: '情天可视化'
    }
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login/Login.vue'),
    meta: {
        title: '登录'
    }
  },
  {
    path: '/visualization',
    name: 'visualization',
    component: () => import('../views/Visualization/Visualization.vue'),
    meta: {
      title: '数据可视化'
    },
    children: [
      {
        path: 'list',
        name: 'list',
        component: () => import('../views/Visualization/view/List.vue'),
        meta: {
          title: '数据可视化'
        },
      }
    ]
  },
  {
    path: '/designer',
    name: 'designer',
    component: () => import('../views/Designer/Designer.vue'),
    meta: {
      title: '数据大屏设计'
    },
  },
  {
    path: '/preview',
    name: 'preview',
    component: () => import('../views/Preview/Preview.vue'),
    meta: {
      title: '大屏预览'
    },
  },
  {
    path: '/release',
    name: 'release',
    component: () => import('../views/Release/Release.vue'),
    meta: {
      title: '大屏发布'
    },
  },
  {
    path: '/template',
    name: 'template',
    component: () => import('../views/Template/Template.vue'),
    meta: {
      title: '大屏模板预览'
    },
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})


router.beforeEach((to, from, next) => {
  // if (from.meta.title) {
  //   document.title = from.meta.title;
  // }
  /* 路由发生变化修改页面title */

  if (to.meta.title) {
    document.title = to.meta.title;
  }
  if (to.path === '/login') return next() //登录页面，直接跳转
  if (to.path === '/release') return next() //大屏发布页面，直接跳转
  if (to.path === '/template') return next() //模板预览页面，直接跳转
  let token = window.localStorage.getItem('token')||''
  if(!token) {
    return next('/login')
  }
  // return next()
  
  return next();
})

export default router
