@echo off
set port=8082
for /f "tokens=5" %%i in ('netstat -ano^|findstr ":%port%"') do (
    TaskKill.exe /pid %%i -t -f
)
java -jar -Dspring.config.location=application.properties keshihua.jar
pause