/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.7.26 : Database - keshihua_open
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`keshihua_open` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `keshihua_open`;

/*Table structure for table `data_link` */

DROP TABLE IF EXISTS `data_link`;

CREATE TABLE `data_link` (
  `data_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_link_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属分组id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `type` char(20) NOT NULL DEFAULT '' COMMENT '类型(数据连接类型)',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态：1正常 2异常',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8 COMMENT='数据连接data_link';

/*Data for the table `data_link` */

insert  into `data_link`(`data_link_id`,`data_link_group_id`,`name`,`type`,`status`,`user_id`,`add_time`,`update_time`) values (149,0,'sqlserver数据','sqlserver',1,4,'2021-02-26 18:38:39','2021-12-20 14:23:22'),(152,0,'Oracle','oracle',1,4,'2021-03-08 17:35:34','2021-12-20 14:23:03'),(172,0,'mysql数据源','mysql',1,4,'2021-12-20 14:23:14','2000-01-01 01:00:00'),(173,0,'excel数据','excel',1,4,'2021-12-20 14:23:34','2000-01-01 01:00:00');

/*Table structure for table `data_link_excel` */

DROP TABLE IF EXISTS `data_link_excel`;

CREATE TABLE `data_link_excel` (
  `data_link_excel_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `data_link_id` int(11) NOT NULL DEFAULT '0' COMMENT 'data_link表主键',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `file_path` varchar(100) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_name` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名称',
  `table_name` varchar(50) NOT NULL DEFAULT '' COMMENT '对应的数据库表名',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_excel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='数据连接扩展表data_link_excel';

/*Data for the table `data_link_excel` */

insert  into `data_link_excel`(`data_link_excel_id`,`data_link_id`,`user_id`,`file_path`,`file_name`,`table_name`,`add_time`,`update_time`) values (29,173,4,'','','','2021-12-20 14:23:34','2000-01-01 01:00:00');

/*Table structure for table `data_link_group` */

DROP TABLE IF EXISTS `data_link_group`;

CREATE TABLE `data_link_group` (
  `data_link_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分组主键id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分组名称',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据源分组';

/*Data for the table `data_link_group` */

/*Table structure for table `data_link_mysql` */

DROP TABLE IF EXISTS `data_link_mysql`;

CREATE TABLE `data_link_mysql` (
  `data_link_mysql_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `data_link_id` int(11) NOT NULL DEFAULT '0' COMMENT 'data_link表主键',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `host` varchar(100) NOT NULL DEFAULT '' COMMENT '服务host地址',
  `port` varchar(20) NOT NULL DEFAULT '' COMMENT '端口port',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库账号',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '数据库密码',
  `default_database` varchar(50) NOT NULL DEFAULT '' COMMENT '默认选择的数据库',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_mysql_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

/*Data for the table `data_link_mysql` */

insert  into `data_link_mysql`(`data_link_mysql_id`,`data_link_id`,`user_id`,`host`,`port`,`username`,`password`,`default_database`,`add_time`,`update_time`) values (72,172,4,'','','','','','2021-12-20 14:23:14','2000-01-01 01:00:00');

/*Table structure for table `data_link_oracle` */

DROP TABLE IF EXISTS `data_link_oracle`;

CREATE TABLE `data_link_oracle` (
  `data_link_oracle_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `data_link_id` int(11) NOT NULL DEFAULT '0' COMMENT 'data_link表主键',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `host` varchar(100) NOT NULL DEFAULT '' COMMENT '服务host地址',
  `port` varchar(20) NOT NULL DEFAULT '' COMMENT '端口port',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库账号',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '数据库密码',
  `server_name` varchar(50) NOT NULL DEFAULT '' COMMENT '服务名',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_oracle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `data_link_oracle` */

insert  into `data_link_oracle`(`data_link_oracle_id`,`data_link_id`,`user_id`,`host`,`port`,`username`,`password`,`server_name`,`add_time`,`update_time`) values (39,152,4,'','','','','','2021-03-08 17:35:34','2021-12-20 14:23:03');

/*Table structure for table `data_link_sqlserver` */

DROP TABLE IF EXISTS `data_link_sqlserver`;

CREATE TABLE `data_link_sqlserver` (
  `data_link_sqlserver_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `data_link_id` int(11) NOT NULL DEFAULT '0' COMMENT 'data_link表主键',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `host` varchar(100) NOT NULL DEFAULT '' COMMENT '服务host地址',
  `port` varchar(20) NOT NULL DEFAULT '' COMMENT '端口port',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库账号',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '数据库密码',
  `default_database` varchar(50) NOT NULL DEFAULT '' COMMENT '默认选择的数据库',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_link_sqlserver_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `data_link_sqlserver` */

insert  into `data_link_sqlserver`(`data_link_sqlserver_id`,`data_link_id`,`user_id`,`host`,`port`,`username`,`password`,`default_database`,`add_time`,`update_time`) values (7,149,4,'127.0.0.1','1433','huangjun','123456','huangjun','2021-02-26 18:38:39','2021-12-20 14:23:22');

/*Table structure for table `data_model` */

DROP TABLE IF EXISTS `data_model`;

CREATE TABLE `data_model` (
  `data_model_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` char(32) NOT NULL DEFAULT '' COMMENT '使用uid作为 数据模型 的查询主键',
  `data_model_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属分组id',
  `data_link_id` int(11) NOT NULL DEFAULT '0' COMMENT '选择的数据连接id',
  `data_link_table` varchar(50) NOT NULL DEFAULT '' COMMENT '选择的数据连接中的表名',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `table_name` varchar(30) NOT NULL DEFAULT '' COMMENT '在用户专属数据库中的表名',
  `table_change_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据模型表状态：1、不变 2、已经变了   当用户重新选择数据源的不同的表示，状态为变为2，在抽取数据时，会根据该状态选择是否需要删除数据模型表',
  `extract_rule` text COMMENT '抽取规则：json格式',
  `extract` tinyint(4) NOT NULL DEFAULT '1' COMMENT '抽取方式：1覆盖 2追加',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据预览状态：1 没有  2有且成功  只有成功预览数据的数据模型，才可以进行数据抽取',
  `timeout_type` char(5) NOT NULL DEFAULT 'false' COMMENT '是否定时抽取数据:false 否 true 是',
  `timeout_unit` int(11) NOT NULL DEFAULT '600' COMMENT '定时抽取时间间隔(秒)：',
  `timeout_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '下一次定时抽取数据的时间',
  PRIMARY KEY (`data_model_id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='数据模型';

/*Data for the table `data_model` */

insert  into `data_model`(`data_model_id`,`uid`,`data_model_group_id`,`data_link_id`,`data_link_table`,`user_id`,`name`,`table_name`,`table_change_status`,`extract_rule`,`extract`,`add_time`,`update_time`,`status`,`timeout_type`,`timeout_unit`,`timeout_time`) values (38,'236d63ad901a4250aab96e9b1f14fbb6',0,149,'',4,'测试模型','model_38',1,NULL,1,'2021-12-20 14:23:47','2000-01-01 01:00:00',1,'false',600,'2000-01-01 01:00:00');

/*Table structure for table `data_model_group` */

DROP TABLE IF EXISTS `data_model_group`;

CREATE TABLE `data_model_group` (
  `data_model_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分组主键id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分组名称',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_model_group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据模型分组';

/*Data for the table `data_model_group` */

/*Table structure for table `data_node` */

DROP TABLE IF EXISTS `data_node`;

CREATE TABLE `data_node` (
  `data_node_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name1` varchar(50) NOT NULL DEFAULT '' COMMENT '节点名称',
  `host1` varchar(100) NOT NULL DEFAULT '' COMMENT '数据节点host',
  `port1` varchar(20) NOT NULL DEFAULT '' COMMENT '数据库端口',
  `username1` varchar(50) NOT NULL DEFAULT '' COMMENT '数据节点账号',
  `password1` varchar(100) NOT NULL DEFAULT '' COMMENT '数据节点密码',
  `database1` varchar(50) NOT NULL DEFAULT '' COMMENT '默认选择的数据库',
  `status1` tinyint(4) NOT NULL DEFAULT '1' COMMENT '使用状态:1、可用 2、已满 3、停用',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`data_node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='数据节点data_node，使用mysql做数据节点';

/*Data for the table `data_node` */

insert  into `data_node`(`data_node_id`,`name1`,`host1`,`port1`,`username1`,`password1`,`database1`,`status1`,`add_time`,`update_time`) values (1,'本地1','127.0.0.1','3306','root','root','keshihua',1,'2021-01-13 17:32:11','2000-01-01 01:00:00');

/*Table structure for table `data_visualization` */

DROP TABLE IF EXISTS `data_visualization`;

CREATE TABLE `data_visualization` (
  `data_visualization_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_visualization_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属分组id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '发布状态：1 没有  2有',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型：1 PC端 2 移动端',
  `style` text COMMENT '属性样式集合：json格式',
  `cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面图',
  `release_id` char(32) NOT NULL DEFAULT '' COMMENT '发布id，使用uid作为发布后的id，为了用户信息安全考虑',
  PRIMARY KEY (`data_visualization_id`),
  KEY `release_id` (`release_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='数据可视化';

/*Data for the table `data_visualization` */

/*Table structure for table `data_visualization_group` */

DROP TABLE IF EXISTS `data_visualization_group`;

CREATE TABLE `data_visualization_group` (
  `data_visualization_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分组主键id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分组名称',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '更新时间',
  `visualization_num` int(11) NOT NULL DEFAULT '0' COMMENT '下属数据可视化的数量',
  PRIMARY KEY (`data_visualization_group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据可视化分组';

/*Data for the table `data_visualization_group` */

/*Table structure for table `data_visualization_release` */

DROP TABLE IF EXISTS `data_visualization_release`;

CREATE TABLE `data_visualization_release` (
  `data_visualization_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_visualization_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属分组id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '发布状态：1 没有  2有',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型：1 PC端 2 移动端',
  `style` text COMMENT '属性样式集合：json格式',
  `cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面图',
  `pswd` varchar(200) NOT NULL DEFAULT '' COMMENT '密码：带密码发布时，会有密码',
  PRIMARY KEY (`data_visualization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据可视化--发布表';

/*Data for the table `data_visualization_release` */

/*Table structure for table `data_visualization_template` */

DROP TABLE IF EXISTS `data_visualization_template`;

CREATE TABLE `data_visualization_template` (
  `data_visualization_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_visualization_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属分组id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '发布状态：1 没有  2有',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型：1 PC端 2 移动端 3 PC端响应式',
  `style` text COMMENT '属性样式集合：json格式',
  `cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面图',
  `release_id` char(32) NOT NULL DEFAULT '' COMMENT '发布id，使用uid作为发布后的id，为了用户信息安全考虑',
  PRIMARY KEY (`data_visualization_id`),
  KEY `release_id` (`release_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='数据可视化';

/*Data for the table `data_visualization_template` */

/*Table structure for table `data_visualization_widget` */

DROP TABLE IF EXISTS `data_visualization_widget`;

CREATE TABLE `data_visualization_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '所在分组的id',
  `data_visualization_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属大屏id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `label` varchar(30) NOT NULL DEFAULT '' COMMENT '用来显示文本名称',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '组件名，如：text title zhuxingtu',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '活动状态',
  `draggable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可被拖拽',
  `resizable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可以被改变大小',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT '宽度',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '高度',
  `x` int(11) NOT NULL DEFAULT '0' COMMENT 'x方向的位置',
  `y` int(11) NOT NULL DEFAULT '0' COMMENT 'y方向的位置',
  `px` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `py` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `zindex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【用来图层排序】',
  `showZIndex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【实际用来显示的】',
  `display` varchar(10) NOT NULL DEFAULT 'block' COMMENT '是否隐藏图层：block显示  none隐藏',
  `style` text COMMENT '属性样式集合：json格式',
  `data` text COMMENT '组件的静态数据',
  PRIMARY KEY (`id`),
  KEY `data_visualization_id` (`data_visualization_id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='数据可视化--组件列表';

/*Data for the table `data_visualization_widget` */

/*Table structure for table `data_visualization_widget_release` */

DROP TABLE IF EXISTS `data_visualization_widget_release`;

CREATE TABLE `data_visualization_widget_release` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '所在分组的id',
  `data_visualization_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属大屏id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `label` varchar(30) NOT NULL DEFAULT '' COMMENT '用来显示文本名称',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '组件名，如：text title zhuxingtu',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '活动状态',
  `draggable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可被拖拽',
  `resizable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可以被改变大小',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT '宽度',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '高度',
  `x` int(11) NOT NULL DEFAULT '0' COMMENT 'x方向的位置',
  `y` int(11) NOT NULL DEFAULT '0' COMMENT 'y方向的位置',
  `px` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `py` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `zindex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【用来图层排序】',
  `showZIndex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【实际用来显示的】',
  `display` varchar(10) NOT NULL DEFAULT 'block' COMMENT '是否隐藏图层：block显示  none隐藏',
  `style` text COMMENT '属性样式集合：json格式',
  `data` text COMMENT '组件的静态数据',
  PRIMARY KEY (`id`),
  KEY `data_visualization_id` (`data_visualization_id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据可视化--组件列表--发布表';

/*Data for the table `data_visualization_widget_release` */

/*Table structure for table `data_visualization_widget_template` */

DROP TABLE IF EXISTS `data_visualization_widget_template`;

CREATE TABLE `data_visualization_widget_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '所在分组的id',
  `data_visualization_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属大屏id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  `label` varchar(30) NOT NULL DEFAULT '' COMMENT '用来显示文本名称',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '组件名，如：text title zhuxingtu',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '活动状态',
  `draggable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可被拖拽',
  `resizable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可以被改变大小',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT '宽度',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '高度',
  `x` int(11) NOT NULL DEFAULT '0' COMMENT 'x方向的位置',
  `y` int(11) NOT NULL DEFAULT '0' COMMENT 'y方向的位置',
  `px` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `py` int(11) NOT NULL DEFAULT '0' COMMENT '分组时，相对于父组件的位置',
  `zindex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【用来图层排序】',
  `showZIndex` int(11) NOT NULL DEFAULT '1' COMMENT '图层显示级别【实际用来显示的】',
  `display` varchar(10) NOT NULL DEFAULT 'block' COMMENT '是否隐藏图层：block显示  none隐藏',
  `style` text COMMENT '属性样式集合：json格式',
  `data` text COMMENT '组件的静态数据',
  PRIMARY KEY (`id`),
  KEY `data_visualization_id` (`data_visualization_id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='数据可视化--组件列表';

/*Data for the table `data_visualization_widget_template` */

/*Table structure for table `enums` */

DROP TABLE IF EXISTS `enums`;

CREATE TABLE `enums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `enum_key` varchar(50) NOT NULL DEFAULT '' COMMENT '枚举名(字符串)',
  `key1` varchar(50) NOT NULL DEFAULT '' COMMENT '键名',
  `value1` varchar(100) NOT NULL DEFAULT '' COMMENT '键值',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '排序指',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='枚举表';

/*Data for the table `enums` */

insert  into `enums`(`id`,`name`,`enum_key`,`key1`,`value1`,`sort`,`add_time`,`update_time`) values (1,'数据连接类型','dataLinkType','excel','Excel',1,'2021-01-13 18:18:30','2021-01-13 18:18:30'),(2,'数据连接类型','dataLinkType','mysql','Mysql',1,'2021-01-13 18:18:39','2021-01-13 18:18:39'),(3,'数据连接类型','dataLinkType','oracle','Oracle',1,'2021-01-13 18:18:49','2021-01-13 18:18:49'),(4,'数据连接类型','dataLinkType','sqlserver','SqlServer',1,'2021-01-13 18:19:03','2021-01-13 18:19:03'),(5,'Excel字段类型','excelValueType','number','数字',1,'2021-01-18 11:00:57','2021-01-18 11:00:57'),(6,'Excel字段类型','excelValueType','string','文本数据',2,'2021-01-18 11:00:57','2021-01-18 11:00:57'),(7,'Excel字段类型','excelValueType','date','日期格式(如2020-04-25)',3,'2021-01-18 11:00:57','2021-01-18 11:00:57'),(8,'Excel字段类型','excelValueType','datetime','日期时间格式(如2020-04-25 14:42:36)',4,'2021-01-18 11:00:57','2021-01-18 11:00:57'),(9,'数据可视化类型','visualizationType','1','PC端',1,'2021-04-06 11:57:42','2021-04-06 11:57:42'),(10,'数据可视化类型','visualizationType','2','移动端',1,'2021-04-06 11:57:59','2021-04-06 11:57:59');

/*Table structure for table `geo_area` */

DROP TABLE IF EXISTS `geo_area`;

CREATE TABLE `geo_area` (
  `id` int(10) NOT NULL COMMENT 'ID',
  `areaname` varchar(50) NOT NULL COMMENT '栏目名',
  `parentid` int(10) NOT NULL COMMENT '父栏目',
  `shortname` varchar(50) DEFAULT NULL,
  `lng` varchar(20) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `level` tinyint(1) NOT NULL,
  `position` varchar(255) NOT NULL,
  `sort` tinyint(3) unsigned DEFAULT '50' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='地理数据：省、市、区数据，包含经纬度';

/*Data for the table `geo_area` */

/*Table structure for table `phone_code` */

DROP TABLE IF EXISTS `phone_code`;

CREATE TABLE `phone_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tel` char(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态：1未使用 2已使用',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `phone_code` */

insert  into `phone_code`(`id`,`tel`,`code`,`status`,`add_time`,`update_time`) values (1,'18668049687','554804',1,'2021-09-22 16:48:12','2000-01-01 00:00:00'),(2,'18668049687','024723',2,'2021-09-22 16:36:19','2021-09-22 16:40:50'),(3,'18668049687','615910',2,'2021-09-22 16:49:27','2021-09-22 16:50:06');

/*Table structure for table `test1` */

DROP TABLE IF EXISTS `test1`;

CREATE TABLE `test1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL DEFAULT '',
  `time1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time2` time DEFAULT NULL,
  `time3` datetime DEFAULT NULL,
  `time4` date DEFAULT NULL,
  `num1` float NOT NULL DEFAULT '10.111',
  `num2` double NOT NULL DEFAULT '10.22',
  `num3` decimal(60,10) NOT NULL DEFAULT '1.0000000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `test1` */

insert  into `test1`(`id`,`name`,`time1`,`time2`,`time3`,`time4`,`num1`,`num2`,`num3`) values (1,'第一个','2021-01-17 01:33:51','01:33:51','2021-01-17 01:33:51','2021-01-17',10.111,10.22,'1.0000000000'),(2,'第2个','2021-01-17 01:34:33','01:34:32','2021-01-17 01:34:33','2021-01-17',10.111,10.22,'1.0000000000'),(3,'第3个','2021-01-17 01:51:34','01:51:33','2021-01-17 01:51:34','2021-01-17',10.111,10.22,'1.0000000000'),(4,'第3个','2021-01-17 01:54:23','01:54:22','2021-01-17 01:54:23','2021-01-17',2229.22,65.225448979,'5648.1600000000'),(5,'第3个','2021-01-17 01:56:12','01:56:11','2021-01-17 01:56:12','2021-01-17',0,0,'0.0000000000'),(6,'第4个','2021-01-17 01:59:50','01:59:49','2021-01-17 01:59:50','2021-01-17',21.0001,21.000111,'21.0000000000'),(7,'第6个','2021-01-17 02:05:19','02:05:19','2021-01-17 02:05:19','2021-01-17',21.0001,21.000111,'22222221.0000000000'),(8,'第7个','2021-01-17 02:08:33','02:08:33','2021-01-17 02:08:33','2021-01-17',21.0001,21.000111,'222222222222222229504.0000000000'),(9,'第8个','2021-01-17 02:09:54','02:09:54','2021-01-17 02:09:54','2021-01-17',21.0001,21.000111,'222222224721063182336.0000000000'),(10,'第9个','2021-01-17 02:11:12','02:11:11','2021-01-17 02:11:12','2021-01-17',21.0001,21.000111,'14511112222.0202159882'),(11,'第10个','2021-01-17 15:38:23','15:38:23','2021-01-17 15:38:23','2021-01-17',21.0001,21.000111,'54874.4564000000'),(12,'第10个','2021-01-17 15:38:37','15:38:37','2021-01-17 15:38:37','2021-01-17',21.0001,21.000111,'54874.4564000000'),(13,'第11个','2021-01-17 18:43:16','18:43:15','2018-01-31 21:16:11','2021-01-17',21.0001,21.000111,'54874.4564000000'),(14,'第12个','2021-01-17 18:44:56','18:44:55','2018-01-31 21:16:11','2018-01-31',21.0001,21.000111,'54874.4564000000'),(15,'第13个','2021-01-17 18:48:11','18:48:10','2018-01-30 14:58:00','2018-01-30',21.0001,21.000111,'54874.4564000000'),(16,'第14个','2021-01-17 18:49:58','18:49:58','2018-01-30 14:58:00','2018-01-30',21.0001,21.000111,'54874.4564000000'),(17,'1212','2021-06-23 14:05:25','14:05:25','2018-01-30 14:58:00','2018-01-30',21.0001,21.000111,'54874.4564000000');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户表主键',
  `user` varchar(30) NOT NULL DEFAULT '' COMMENT '账号',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '用户姓名',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '登录密码',
  `sex` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别：1男 2女',
  `phone` char(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '更新时间',
  `data_node_id` int(11) NOT NULL DEFAULT '0' COMMENT '数据节点主键id',
  `level` int(11) NOT NULL DEFAULT '1' COMMENT '会员等级',
  `level_expiration_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '会员过期时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `user` */

insert  into `user`(`user_id`,`user`,`name`,`password`,`sex`,`phone`,`add_time`,`update_time`,`data_node_id`,`level`,`level_expiration_time`) values (4,'admin','管理员','admin',1,'18611111111','2021-01-19 14:57:48','2021-08-31 16:59:03',1,5,'1980-01-01 01:00:00');

/*Table structure for table `user_level` */

DROP TABLE IF EXISTS `user_level`;

CREATE TABLE `user_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '等级名称',
  `data_link_num` int(11) NOT NULL DEFAULT '5' COMMENT '数据源数量限制',
  `excel_size` int(11) NOT NULL DEFAULT '100' COMMENT '单位：KB，上传excel数据源时，excel文件大小限制。',
  `excel_row` int(11) NOT NULL DEFAULT '10000' COMMENT '单位：行数，上传excel数据源时，excel文件行数限制',
  `data_model_num` int(11) NOT NULL DEFAULT '20' COMMENT '数据模型数量限制',
  `data_model_timeout_num` int(11) NOT NULL DEFAULT '0' COMMENT '定时模型数量限制',
  `data_visualization_num` int(11) NOT NULL DEFAULT '1' COMMENT '大屏数量限制',
  `data_visualization_widget` varchar(500) NOT NULL DEFAULT 'all' COMMENT '可以使用的大屏组件',
  `price` int(11) NOT NULL DEFAULT '1' COMMENT '会员价格',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NOT NULL DEFAULT '1980-01-01 01:00:00' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='会员vip';

/*Data for the table `user_level` */

insert  into `user_level`(`id`,`name`,`data_link_num`,`excel_size`,`excel_row`,`data_model_num`,`data_model_timeout_num`,`data_visualization_num`,`data_visualization_widget`,`price`,`add_time`,`update_time`) values (1,'免费版',5,100,10000,20,0,1,'all',0,'2021-08-25 16:18:50','1980-01-01 01:00:00'),(2,'基础版',100,500,500000,200,20,10,'all',98,'2021-08-25 16:52:57','1980-01-01 01:00:00'),(3,'精英版',200,1024,1000000,500,50,30,'all',198,'2021-08-25 16:54:03','1980-01-01 01:00:00'),(4,'豪华版',500,1024,1000000,1000,100,100,'all',398,'2021-08-25 16:54:41','1980-01-01 01:00:00'),(5,'至尊版',2000,1024,1000000,5000,500,500,'all',1998,'2021-08-25 16:55:30','1980-01-01 01:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
